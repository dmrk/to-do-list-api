import { Request, Response } from 'express';
import { Todo } from '../model/todo';
import { readTodoListFromFile, writeTodoListToFile } from '../utils/jsonfile.util';

let todoList: Todo[] = readTodoListFromFile();
let initId = todoList.length > 0 ? todoList[todoList.length - 1].id + 1 : 1;

export const getTodoList = (req: Request, res: Response) => {
    res.json(todoList);
};

export const getTodoById = (req: Request, res: Response) => {
    const { id } = req.params;
    const todo = todoList.find(t => t.id === parseInt(id));
    if (todo) {
        res.json(todo);
    } else {
        res.status(404).json({ message: 'Todo not found' });
    }
};

export const createTodo = (req: Request, res: Response) => {
    const { title } = req.body;
    const newTodo: Todo = {
        id: initId++,
        title,
        completed: false,
        created_date: new Date().toISOString(),
        updated_date: new Date().toISOString()
    };
    todoList.push(newTodo);
    writeTodoListToFile(todoList);
    res.status(201).json(newTodo);
};

export const updateTodo = (req: Request, res: Response) => {
    const { id } = req.params;
    const { title, completed } = req.body;

    const todo = todoList.find(t => t.id === parseInt(id));
    if (todo) {
        todo.title = title ?? todo.title;
        todo.completed = completed ?? todo.completed;
        todo.updated_date = new Date().toISOString();
        writeTodoListToFile(todoList);
        res.json(todo);
    } else {
        res.status(404).json({ message: 'not found' });
    }
};

export const deleteTodo = (req: Request, res: Response) => {
    const { id } = req.params;
    todoList = todoList.filter(t => t.id !== parseInt(id));
    writeTodoListToFile(todoList);
    res.status(204).send();
};
