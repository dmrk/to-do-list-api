export interface Todo {
    id: number;
    title: string;
    completed: boolean;
    created_date: string;
    updated_date: string;
}
