import { Router } from 'express';
import { getTodoList,getTodoById, createTodo, updateTodo, deleteTodo } from '../controller/ToDoList.controller';

const router = Router();

router.get('/to-do-list', getTodoList);
router.get('/to-do-list/:id', getTodoById);
router.post('/to-do-list', createTodo);
router.put('/to-do-list/:id', updateTodo);
router.delete('/to-do-list/:id', deleteTodo);

export default router;
