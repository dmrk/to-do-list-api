# To-do List API

- ทำ REST API โดยใช้ Node JS (JavaScript หรือ TypeScript) สำหรับโปรแกรม To do list โดยห้ามเชื่อมต่อกับ database


## Requirements

- Node.js
- npm

# Install dependencies

npm install


# Run the development server

npm start

The server will start on http://localhost:3000.

